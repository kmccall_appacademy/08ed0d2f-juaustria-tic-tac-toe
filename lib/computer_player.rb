class ComputerPlayer
  attr_reader :name, :board
  attr_accessor :mark
  def initialize(name)
    @name = name
  end

  def get_move
    @possible_moves = []
    @board.grid.each_with_index do |row, idx|
      row.each_with_index do |col, idx2|
        @possible_moves << [idx, idx2] if col.nil?
      end
    end
    # winning move
    @possible_moves.each do |pos|
      @board.grid[pos[0]][pos[1]] = mark
      p "#{name}'s move..." if @board.winner == mark
      if @board.winner == mark
        @board.grid[pos[0]][pos[1]] = nil
        return pos
      end
      @board.grid[pos[0]][pos[1]] = nil
    end
    # random move
    p "#{name}'s move..."
    @possible_moves[rand(0...@possible_moves.length)]
  end

  def display(board)
    @board = board
    @board.grid.each { |row| p row }
  end
end
