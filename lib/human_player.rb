class HumanPlayer
  attr_reader :name
  attr_accessor :mark
  def initialize(name)
    @name = name
  end

  def get_move
    puts "Where would you like to make your move, #{name}? 'row, column' "
    move = gets.chomp
    raise 'Incorrect format' if move.split(',').map(&:to_i).length != 2
    unless @board.nil?
      if move.split(',').map(&:to_i)[0] >= @board.grid.length ||
         move.split(',').map(&:to_i)[1] >= @board.grid.length
        raise 'Incorrect size'
      end
    end
    move.split(',').map(&:to_i)
  end

  def display(board)
    @board = board
    @board.grid.each { |row| p row }
  end
end
