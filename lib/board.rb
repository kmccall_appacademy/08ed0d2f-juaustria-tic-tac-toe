class Board
  attr_reader :grid

  def initialize(grid = nil)
    @grid = if grid.nil?
              Array.new(3) { Array.new(3) }
            else
              grid
            end
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def place_mark(pos, mark)
    raise "#{pos} is already marked with #{mark}" unless empty?(pos)
    self[pos] = mark
  end

  def empty?(pos)
    self[pos].nil?
  end

  def winner
    #### Win conditions assume square grids
    # row
    @grid.each do |row|
      return row[0] if row.uniq.length == 1
    end

    # column
    @grid.each_with_index do |_row, idx|
      column_arr = []
      @grid.each_with_index do |_row, idx2|
        column_arr << @grid[idx2][idx]
      end
      if column_arr.none?(&:nil?) &&
         column_arr.all? { |mark| mark == column_arr[0] }
        return column_arr[idx]
      end
    end

    # left diagonal
    diagonal_arr = []
    @grid.each_with_index do |_row, idx|
      diagonal_arr << @grid[idx][idx]
    end
    if diagonal_arr.none?(&:nil?) &&
       diagonal_arr.all? { |mark| mark == diagonal_arr[0] }
      return diagonal_arr[0]
    end

    # right diagonal
    diagonal_arr = []
    @grid.each_with_index do |_row, idx|
      diagonal_arr << @grid[idx][-1 - idx]
    end
    if diagonal_arr.none?(&:nil?) &&
       diagonal_arr.all? { |mark| mark == diagonal_arr[0] }
      return diagonal_arr[0]
    end

    # no winner
    nil
  end

  def over?
    return true unless winner.nil?
    @grid.each do |row|
      return false if row.any?(&:nil?)
    end
  end

end
