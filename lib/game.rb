require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_reader :player_one, :player_two, :board
  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @player_one.mark = :X
    @player_two.mark = :O
    @current_player = player_one
    @board = Board.new
  end

  def current_player
    @current_player
  end

  def switch_players!
    @current_player = if @current_player == player_one
                        player_two
                      else
                        player_one
                      end
  end

  def play_turn
    @current_player.display(@board)
    @board.place_mark(@current_player.get_move, @current_player.mark)
    switch_players!
  end

  def play
    play_turn until board.over?
    @board.grid.each { |row| p row }
    winning_player = if @board.winner == @player_one.mark
                       @player_one.name
                     elsif @board.winner == @player_two.mark
                       @player_two.name
                     else
                       'Nobody'
                     end
    p "#{winning_player} wins!"
  end
end
